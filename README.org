:PROPERTIES:
:CREATED:  [2023-10-01 Sun 19:26]
:LAST_MODIFIED: [2023-10-01 Sun 19:37]
:END:

Install:
#+begin_src shell
  git clone https://codeberg.org/seigakaku/simple-x11-xlib-mouse-disable-while-typing
  cd simple-x11-xlib-mouse-disable-while-typing
  make
#+end_src

You will need ~pkg-config~ Xlib, Xinput2 and XTest.
On ~Arch Linux~ those are all part of the ~xorg-server~ package, ~xorg-xinput~ will also be useful.

Usage:
#+begin_src shell
  ./disabler <Device ID to disable> [Delay between checks]
#+end_src

Example:

Say I have a mouse and a touchpad connected, and want to disable both when typing.
We can find the device ID's using the xinput command line tool:

#+begin_src shell
  xinput
#+end_src

#+RESULTS:
| ⎡ Virtual core pointer                          | id=2  | [master pointer  (3)] |
| ⎜   ↳ Virtual core XTEST pointer               | id=4  | [slave  pointer  (2)] |
| ⎜   ↳ Logitech G300s Optical Gaming Mouse      | id=13 | [slave  pointer  (2)] |
| ⎜   ↳ Elan Touchpad                            | id=10 | [slave  pointer  (2)] |
| ⎣ Virtual core keyboard                         | id=3  | [master keyboard (2)] |
| ↳ Virtual core XTEST keyboard                  | id=5  | [slave  keyboard (3)] |
| ↳ Logitech G300s Optical Gaming Mouse Keyboard | id=12 | [slave  keyboard (3)] |
| ↳ AT Translated Set 2 keyboard                 | id=11 | [slave  keyboard (3)] |
| ↳ Ideapad extra buttons                        | id=9  | [slave  keyboard (3)] |
| ↳ Power Button                                 | id=8  | [slave  keyboard (3)] |
| ↳ Video Bus                                    | id=7  | [slave  keyboard (3)] |
| ↳ Power Button                                 | id=6  | [slave  keyboard (3)] |

As we can see, there's two actual pointer devices:
~Logitech G300s Optical Gaming Mouse~ and ~Elan Touchpad~, with IDs 13 and 10 respectively.

So we'll run:

#+begin_src shell
  ./disabler 13,10
#+end_src

By default keypresses will be checked every ~250000000~ nanoseconds (~0.25~ seconds),
to change it pass the second parameter as nanoseconds.

#+begin_src shell
  ./disabler 13,10 100000000 # 0.1 seconds
#+end_src

This was particularly useful to me on OpenBSD, where my hardware didn't have appropriate drivers
and I couldn't get "disable-while-typing" functionality to work properly, which is massively annoying.
