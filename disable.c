/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details. */

#include <X11/Xatom.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/record.h>
#include <alloca.h>
#include <ctype.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define MAX_DEV_COUNT (32)

static volatile int enabled = 1;
static uint64_t delay = 250000000;

static pthread_t thread;
static Display* dpy = NULL;
static uint8_t dev_count = 0;
static uint8_t devices[MAX_DEV_COUNT];
static Atom props[MAX_DEV_COUNT];

static inline Atom make_prop(Display *dpy) {
  Atom prop = XInternAtom(dpy, "Device Enabled", False);
  if (prop == None) {
    fprintf(stderr, "Invalid property or device.\n");
    _exit(1);
  }
  return prop;
}

static void do_toggle(int32_t enable) {
  unsigned char *data = alloca(sizeof(int32_t));
  data[0] = enable;
  for (uint8_t i = 0; i < dev_count; ++i) {
    XIChangeProperty(dpy, devices[i], props[i], XA_INTEGER, 8, PropModeReplace, data, 1);
  }
  XSync(dpy, False);
}

static void key_pressed_cb(__attribute__((unused)) XPointer arg, XRecordInterceptData *d) {
  if (d->category != XRecordFromServer) {
    return;
  }

  switch (((unsigned char *)d->data)[0] & 0x7F) {
  case KeyPress:
    enabled = 0;
    break;
  }

  XRecordFreeData(d);
}

static void start_scan() {
  Display* dpy = XOpenDisplay(NULL);

  XRecordRange *rr = XRecordAllocRange();
  XRecordClientSpec rcs = XRecordAllClients;

  rr->device_events.first = KeyPress;
  rr->device_events.last = KeyPress;
  XRecordContext rc = XRecordCreateContext(dpy, 0, &rcs, 1, &rr, 1);
  XFree(rr);
  XRecordEnableContext(dpy, rc, key_pressed_cb, NULL);
}

void *start_thread(__attribute__((unused)) void *data) {
  dpy = XOpenDisplay(NULL);

  struct timespec ts;
  ts.tv_sec = 0;
  ts.tv_nsec = delay;

  for (uint8_t i = 0; i < dev_count; ++i) {
    props[i] = make_prop(dpy);
  }

  do {
    nanosleep(&ts, NULL);
    if (!enabled) {
      enabled = 1;
      do_toggle(0);
      nanosleep(&ts, NULL);
      // If XRecord hasn't gotten a keypress, enabled will still be 1
      if (enabled)
        do_toggle(1);
    }
  } while (1);
}

void sigint_handler(int signo) {
  if (signo == SIGINT) {
    if (dpy) {
      do_toggle(1);
    }
    pthread_kill(thread, signo);
    _exit(0);
  }
}

uint8_t parse_devices(char* spec, uint8_t (*devices)[MAX_DEV_COUNT]) {
  char* p = strtok(spec, ", ");
  uint8_t dev_count = 0;
  do {
    (*devices)[dev_count++] = atoi(p);
    p = strtok(NULL, ", ");
  } while (p && dev_count < MAX_DEV_COUNT);
  return dev_count;
}

int main(int argc, char **argv) {

  if (argc == 1) {
    fprintf(stderr, "Missing device ID argument (see xinput(1))\n");
    _exit(1);
  }
  if (argc >= 2) {
    dev_count = parse_devices(argv[1], &devices);
  }
  if (argc >= 3) {
    delay = atoi(argv[2]);
  }

  if (signal(SIGINT, sigint_handler) == SIG_ERR) {
    fputs("Failed to establish SIGINT handler, forceful termination might leave device disabled.", stderr);
  }

  pthread_create(&thread, NULL, start_thread, NULL);
  start_scan();
  return 0;
}
