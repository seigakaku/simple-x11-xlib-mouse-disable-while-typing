CFLAGS := $(shell pkg-config --cflags xi x11 xtst) \
					-std=c99 \
					-Wall -Wextra -pedantic \
					-O2 -g0 \
					-fPIE -fstack-protector \
					-D_POSIX_C_SOURCE=199506L -D_FORTIFY_SOURCE=2 

LDFLAGS := $(shell pkg-config --libs xi x11 xtst)

.PHONY: clean

disabler: disable.c | Makefile
	$(CC) $(CFLAGS) $? -o $@ $(LDFLAGS)
	strip -v $@

clean:
	rm -f disabler
